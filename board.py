##
## Board Object
##

class Board:
    def __init__(self, num_rows, num_cols):
        self.num_rows = num_rows
        self.num_cols = num_cols

    def rows(self):
        """ Return rows of board """
        return self.num_rows

    def cols(self):
        """ Return cols of board """
        return self.num_cols
