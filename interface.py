## Robot Challenge
##        _____
##       .'/L|__`.
##      / =[_]O|` \
##      |"+_____":|
##    __:='|____`-:__
##   ||[] ||====| []||
##   ||[] | |=| | []||
##   |:||_|=|U| |_||:|
##   |:|||]_=_ =[_||:| LS
##   | |||] [_][]C|| |
##   | ||-'"""""`-|| |
##   /|\\_\_|_|_/_//|\
##  |___|   /|\   |___|   R2 Astromech Droid (1)
##  `---'  |___|  `---'       - Front View -
##         `---'

from robot import compass, Board, Robot
import pdb

def interface(robot, cmd): 
    if (cmd[0] == 'PLACE'):
        if cmd[1] and cmd[2] and cmd[3]: 
            if cmd[3] in compass:
                try:
                    robot.place(int(cmd[1]), int(cmd[2]), cmd[3])
                except ValueError:
                    print("Invalid Arguments. PLACE INT INT " +\
                                        "NORTH|SOUTH|EAST|WEST")

    elif (cmd[0] == 'MOVE'):
        robot.move()
    elif (cmd[0] == 'LEFT'):
        robot.left()
    elif (cmd[0] == 'RIGHT'):
        robot.right()
    elif (cmd[0] == 'REPORT'):
        print(robot.report())
    elif (cmd[0] == 'EXIT'):
        return  True
    else:
        print("""
                Enter a valid command:
                PLACE X Y FACING
                MOVE
                LEFT
                RIGHT
                REPORT
                EXIT
              """)
    return False 
    
    
if __name__ == '__main__':
    board = Board(5,5)
    walle = Robot(board)

    exit_flag = False 
    while not exit_flag:
        command = raw_input("Command? ").rstrip().split()
        exit_flag = interface(walle, command)
    
