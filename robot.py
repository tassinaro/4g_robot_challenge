from board import Board

## 
## Robot Object
## 

compass = ['NORTH', 'EAST', 'SOUTH', 'WEST']

class Robot:

    def __init__(self, board = Board(1, 1)):
        self.pos_x = -1 
        self.pos_y = -1 
        self.facing = 'NORTH' 
        self.board = board
        self.is_on_board = False

    def place(self, x,y,f):
        """ Place robot at a position and direction facing """

        self.is_on_board = True
        self.position(x,y,f)
    
    def move(self):
        """ Move robot one (1) unit in direction it is facing """

        x = self.pos_x
        y = self.pos_y
        f = self.facing

        if f == 'NORTH':
            self.position(x, y+1, f)
        elif f == 'SOUTH':
            self.position(x, y-1, f)
        elif f == 'EAST':
            self.position(x+1, y, f)
        elif f == 'WEST':
            self.position(x-1, y, f)
    
    def left(self):
        """ Rotate robot (90) degrees counterclockwise """

        if self.is_on_board:
            self.rotate('LEFT')
        
    def right(self):
        """ Rotate robot (90) degrees clockwise """

        if self.is_on_board:
            self.rotate('RIGHT')

    def report(self):
        """ Return a status of position and facing """

        if self.is_on_board:
            out = "X: {}, Y: {}, F: {}".format(self.pos_x, 
                                               self.pos_y, 
                                               self.facing)
        else:
            out = ''
        return out

    def rotate(self, direction):
        """ Rotate function that left and right calls use """

        f = self.facing
        if direction == 'LEFT':
            self.facing = compass[compass.index(f)-1]
        elif direction == 'RIGHT':
            #mod use allows us to rotate indefinitely 
            #on the compass
            new_idx = ((compass.index(f)+1) % 4)
            self.facing = compass[new_idx]
            

    def position(self, x,y,f):
        """ Conditional setting of coordinates """

        if self.is_valid_coordinates(x, y):
            self.pos_x = x
            self.pos_y = y
            self.facing = f

    def is_valid_coordinates(self, new_x, new_y):
        """ Validation of proposed coordinates """

        if self.is_on_board:
            if 0 <= new_x < self.board.cols():
                 if 0 <= new_y < self.board.rows():
                     return True
        return False


if __name__ == '__main__':

    #General setup
    board = Board(5,5)
    walle = Robot(board)

    #Test not on board 
    print(walle.report()) #ignored, not placed
    walle.move()
    print(walle.report()) #ignored, not placed
    walle.move()

    #Test place
    print(walle.report()) #X: -1, Y: -1, F: NORTH
    walle.place(5, 5,'NORTH')
    print(walle.report()) #X: -1, Y: -1, F: NORTH
    walle.place(3, 4,'NORTH') 
    print(walle.report()) #X: 3, Y: 4, F: NORTH

    #Test move 
    walle.place(0, 0,'NORTH')
    print(walle.report()) #X:0, Y:0  F:NORTH
    walle.move()
    print(walle.report()) #X:0, Y:1, F:NORTH
    walle.move()
    print(walle.report()) #X:0, Y:2, F:NORTH
    walle.move()
    print(walle.report()) #X:0, Y:3, F:NORTH
    walle.move()
    print(walle.report()) #X:0, Y:4, F:NORTH
    walle.move() #Expect to be ignored since invalid
    print(walle.report()) #X:0, Y:4, F:NORTH 
    
    #Test rotate
    walle.place(0, 0,'NORTH')
    print(walle.report()) #X:0, Y:0, F:NORTH
    walle.left()
    print(walle.report()) #X:0, Y:0, F:WEST
    walle.left()
    print(walle.report()) #X:0, Y:0, F:SOUTH
    walle.left()
    print(walle.report()) #X:0, Y:0, F:EAST
    walle.left()
    print(walle.report()) #X:0, Y:0, F:NORTH
    walle.left()
    print(walle.report()) #X:0, Y:0, F:WEST
    walle.right()
    print(walle.report()) #X:0, Y:0, F:NORTH
    walle.right()
    print(walle.report()) #X:0, Y:0, F:EAST
    walle.right()
    print(walle.report()) #X:0, Y:0, F:SOUTH
    walle.right()
    print(walle.report()) #X:0, Y:0, F:WEST
    walle.right()
    print(walle.report()) #X:0, Y:0, F:NORTH

    #Test rotate and move - walk the perimeter
    walle.place(0, 0,'NORTH')

    for _ in range(4):
        walle.move()
        print(walle.report())

    for _ in range(3):
        walle.right()
        print(walle.report())

        for _ in range(4):
            walle.move()
            print(walle.report())

    #Expected Results:
    #X: 0, Y: 0, F: NORTH
    #X: 0, Y: 1, F: NORTH
    #X: 0, Y: 2, F: NORTH
    #X: 0, Y: 3, F: NORTH
    #X: 0, Y: 4, F: NORTH
    #X: 0, Y: 4, F: EAST
    #X: 1, Y: 4, F: EAST
    #X: 2, Y: 4, F: EAST
    #X: 3, Y: 4, F: EAST
    #X: 4, Y: 4, F: EAST
    #X: 4, Y: 4, F: SOUTH
    #X: 4, Y: 3, F: SOUTH
    #X: 4, Y: 2, F: SOUTH
    #X: 4, Y: 1, F: SOUTH
    #X: 4, Y: 0, F: SOUTH
    #X: 4, Y: 0, F: WEST
    #X: 3, Y: 0, F: WEST
    #X: 2, Y: 0, F: WEST
    #X: 1, Y: 0, F: WEST
    #X: 0, Y: 0, F: WEST

